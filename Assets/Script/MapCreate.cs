﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreate : MonoBehaviour {
	public GameObject TilePrefab;
	public GameObject FloorParent;

	private const int _mapWidth = 12;
	private const int _mapHeight = 12;
	private Vector3 _mapCenter = Vector3.zero;

	void Start () {
		GameObject temp;

		float halfWidth = ((float)_mapWidth / 2) - 0.5f;
		float halfHeight = ((float)_mapHeight / 2) - 0.5f;
		float tileLength = TilePrefab.GetComponent<BoxCollider>().size.x;

		for (int i=0; i< _mapWidth; i++) {
			for (int j=0; j< _mapHeight; j++) {
				temp = Instantiate(TilePrefab, new Vector3(_mapCenter.x + (i - halfWidth) * tileLength, _mapCenter.y, _mapCenter.z + (j - halfHeight) * tileLength), Quaternion.identity) as GameObject;
				temp.transform.parent = FloorParent.transform;
			}
		}
	}
}
