﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickFloor : MonoBehaviour {
	public Material tileMaterial;
	public Material highlightMaterial;
	public Material moveMaterial;

	private Vector3 mousePos;

	private GameObject prevTile = null;

	void Update() {
		if (mousePos != Input.mousePosition) {
			mousePos = Input.mousePosition;

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;

			if (Physics.Raycast(ray, out hitInfo)) {
				if (hitInfo.collider.gameObject.tag == "Floor") {
					if (prevTile != null) {
						ResetMaterial(prevTile);
					}
					SetHighlight(hitInfo.collider.gameObject);
					prevTile = hitInfo.collider.gameObject;
				}
			}
		}

		if (Input.GetMouseButtonDown(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;

			if (Physics.Raycast(ray, out hitInfo)) {
				if (hitInfo.collider.gameObject.tag == "Floor") {
					Debug.Log("tag");
				}
			}
		}
	}

	private void ResetMaterial(GameObject Tile) {
		Tile.GetComponent<MeshRenderer>().material = tileMaterial;
	}

	private void SetHighlight(GameObject Tile) {
		Tile.GetComponent<MeshRenderer>().material = highlightMaterial;
	}
}